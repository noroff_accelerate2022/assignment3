INSERT INTO franchise(description, name) VALUES
('big boi', 'Disney'),
('superheroes', 'Marvel'),
('everything', 'Warner Bros'),
('', 'Euro Film Funding');

INSERT INTO character(alias, full_name, gender, picture) VALUES
('Terminator', 'Arnold Schwarzenegger', 'MALE', 'https://www.pic.com/arnie.jpg'),
('Jack Sparrow', 'Johnny Depp', 'MALE', 'https://www.pic-click.com/sparrow-rum.jpg'),
('Hermione', 'Emma Watson', 'FEMALE', 'https://www.photo.com/hermiona.png');

INSERT INTO movie(director, genre, picture, release_year, title, trailer, franchise_id) VALUES
('Gore Verbinski', 'Adventure', 'https://www.imdb.com/pirates.jpg', 2003, 'Pirates of Caribbean', 'https://www.imdb.com/pirates.jpg', 1),
('James Cameron', 'Action', 'https://www.files.lol/arnie', 1994, 'Terminator', 'https://www.files.lol/arnie', 4),
('Chris Columbus', 'Family, Adventure', 'https://www.files.lol/harry', 2004, 'Harry Potter and the Sorceres stone', 'https://www.files.lol/harry-movie', 3);