package no.noroff.accelerate22.assignment3.mappers;

import no.noroff.accelerate22.assignment3.models.DTOs.MovieDTO;
import no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs.MovieCharacterDTO;
import no.noroff.accelerate22.assignment3.models.entities.Character;
import no.noroff.accelerate22.assignment3.models.entities.Franchise;
import no.noroff.accelerate22.assignment3.models.entities.Movie;
import no.noroff.accelerate22.assignment3.services.CharacterService;
import no.noroff.accelerate22.assignment3.services.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    CharacterService characterService;
    @Autowired
    FranchiseService franchiseService;

    @Mapping(target="characters", source="characters", qualifiedByName = "charactersToIds")
    @Mapping(target="franchise", source="franchise.id")
    public abstract MovieDTO movieToMovieDto(Movie movie);
    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);

    @Mapping(target="characters", source="characters", qualifiedByName ="characterIdsToCharacters")
    @Mapping(target="franchise", source="franchise", qualifiedByName = "franchiseIdToFranchise")
    public abstract Movie movieDtoToMovie(MovieDTO movieDto);

    @Named("charactersToIds")
    Set<MovieCharacterDTO> map(Set<Character> source) {
        if (source == null) return null;
        return source.stream().map(ch -> {
            MovieCharacterDTO dto = new MovieCharacterDTO();
            dto.setId(ch.getId());
            return dto;
        }).collect(Collectors.toSet());
    }

    @Named("franchiseIdToFranchise")
    Franchise mapIdToFranchise(int id) {
        return franchiseService.findById(id);
    }

    @Named("characterIdsToCharacters")
    Set<Character> mapIdsToCharacters(Set<MovieCharacterDTO> source) {
        return source.stream().map(dto -> characterService.findById(dto.getId())).collect(Collectors.toSet());
    }
}
