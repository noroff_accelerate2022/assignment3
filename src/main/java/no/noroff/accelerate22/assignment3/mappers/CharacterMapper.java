package no.noroff.accelerate22.assignment3.mappers;

import no.noroff.accelerate22.assignment3.models.DTOs.CharacterDTO;
import no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs.CharacterMovieDTO;
import no.noroff.accelerate22.assignment3.models.entities.Character;
import no.noroff.accelerate22.assignment3.models.entities.Movie;
import no.noroff.accelerate22.assignment3.services.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    private MovieService movieService;

    @Mapping(target = "movies", source="movies", qualifiedByName = "moviesToIds")
    public abstract CharacterDTO characterToCharacterDto(Character character);
    public abstract Collection<CharacterDTO> characterToCharacterDto(Collection<Character> characters);
    @Mapping(target="movies", source="movies", qualifiedByName = "moviesIdsToMovies")
    public abstract Character characterDtoToCharacter(CharacterDTO characterDto);
    public abstract Collection<Character> characterDtoToCharacter(Set<CharacterDTO> characterDto);

    @Named("moviesToIds")
    Set<CharacterMovieDTO> map(Set<Movie> source) {
        if (source == null) return null;
        return source.stream().map(m -> {
            CharacterMovieDTO dto = new CharacterMovieDTO();
            dto.setId(m.getId());
            return dto;
        }).collect(Collectors.toSet());
    }

    @Named("moviesIdsToMovies")
    Set<Movie> mapIdsToMovies(Set<CharacterMovieDTO> source) {
        return source.stream().map(dto -> movieService.findById(dto.getId())).collect(Collectors.toSet());
    }
}
