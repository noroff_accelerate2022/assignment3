package no.noroff.accelerate22.assignment3.mappers;

import no.noroff.accelerate22.assignment3.models.DTOs.FranchiseDTO;
import no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs.FranchiseMovieDTO;
import no.noroff.accelerate22.assignment3.models.entities.Franchise;
import no.noroff.accelerate22.assignment3.models.entities.Movie;
import no.noroff.accelerate22.assignment3.services.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    MovieService movieService;

    @Mapping(target="movies", source="movies", qualifiedByName = "moviesToMovieIds")
    public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);
    public abstract Collection<FranchiseDTO> franchiseToFranchiseDto(Collection<Franchise> franchises);
    @Mapping(target="movies", source="movies", qualifiedByName = "movieIdsToMovies")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDTO franchiseDto);

    @Named("moviesToMovieIds")
    Set<FranchiseMovieDTO> mapMoviesToIds(Set<Movie> source) {
        if (source == null) return null;
        return source.stream().map(m -> {
            FranchiseMovieDTO dto = new FranchiseMovieDTO();
            dto.setId(m.getId());
            return dto;
        }).collect(Collectors.toSet());
    }

    @Named("movieIdsToMovies")
    Set<Movie> mapMovieIdsToMovies(Set<FranchiseMovieDTO> source) {
        if (source == null) return null;
        return source.stream().map(dto -> movieService.findById(dto.getId())).collect(Collectors.toSet());
    }
}
