package no.noroff.accelerate22.assignment3.services;

import no.noroff.accelerate22.assignment3.models.entities.Character;
import no.noroff.accelerate22.assignment3.models.entities.Franchise;
import no.noroff.accelerate22.assignment3.models.entities.Movie;

import java.util.Set;

public interface MovieService extends CrudService<Movie, Integer> {
    public void setMovieFranchise(int movieId, Franchise franchise);
    public void updateCharacters(int movieId, Set<Character> characters);
    public Set<Character> findAllCharacters(int movieId);
}
