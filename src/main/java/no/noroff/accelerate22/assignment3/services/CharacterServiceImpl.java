package no.noroff.accelerate22.assignment3.services;

import no.noroff.accelerate22.assignment3.models.entities.Character;
import no.noroff.accelerate22.assignment3.models.entities.Movie;
import no.noroff.accelerate22.assignment3.utils.exceptions.CharacterNotFoundException;
import no.noroff.accelerate22.assignment3.repositories.CharacterRepository;
import no.noroff.accelerate22.assignment3.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRepository;
    private final MovieRepository movieRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository, MovieRepository movieRepository) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
    }

    /**
     * Updates movie list that a character played in, as well as character references in each movie.
     *
     * @param characterId a character whose movies should be updated
     * @param movies a set of movies that should replace the old reference
     */
    @Override
    public void updateMovies(int characterId, Set<Movie> movies) {
        Character character = characterRepository.findById(characterId).orElseThrow(() -> new CharacterNotFoundException(characterId));
        character.setMovies(movies);
        // add character reference to each movie in the set
        character.getMovies().forEach(m -> {
            Set<Character> chars = m.getCharacters();
            if (chars == null) {
                chars = Set.of(character);
            } else {
                chars.add(character);
            }
            m.setCharacters(chars);
            movieRepository.save(m);
        });
        characterRepository.save(character);
    }

    /**
     * Finds a character based on its ID.
     *
     * @param id the character's ID
     * @return Character object
     */
    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(() -> new CharacterNotFoundException(id));
    }

    /**
     * Finds all characters from the character table.
     *
     * @return A collection of Character objects.
     */
    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    /**
     * Adds a new character
     *
     * @param character a Character object representing the new tuple
     * @return The Character object
     */
    @Override
    public Character add(Character character) {
        return characterRepository.save(character);
    }

    /**
     * Updates an existing character
     *
     * @param character a Character object containing new values
     * @return the updated Character object
     */
    @Override
    public Character update(Character character) {
        return characterRepository.save(character);
    }

    /**
     * Deletes by character's ID
     *
     * @param id
     */
    @Override
    public void deleteById(Integer id) {
        deleteCharacterReference(id);
        characterRepository.deleteById(id);
    }

    /**
     * Deletes a character based on its object representation
     *
     * @param entity
     */
    @Override
    public void delete(Character entity) {
        deleteCharacterReference(entity.getId());
        characterRepository.delete(entity);
    }

    /**
     * Finds all movies that a given character played in
     *
     * @param characterId the character ID whose movies should be found
     * @return a set of Movie objects
     */
    @Override
    public Set<Movie> findAllMovies(int characterId) {
        Character character = characterRepository.findById(characterId).orElseThrow(() -> new CharacterNotFoundException(characterId));
        return character.getMovies();
    }

    /**
     * Deletes character references from all character's Movies in case of Character deletion.
     *
     * @param characterId character ID that is being deleted
     */
    private void deleteCharacterReference(int characterId) {
        Character character = characterRepository.findById(characterId).orElseThrow(() -> new CharacterNotFoundException(characterId));
        character.getMovies().forEach(movie -> {
            Set<Character> chars = movie.getCharacters();
            chars.remove(character);
            movie.setCharacters(chars);
            movieRepository.save(movie);
        });
    }
}
