package no.noroff.accelerate22.assignment3.services;

import no.noroff.accelerate22.assignment3.models.entities.Franchise;
import no.noroff.accelerate22.assignment3.models.entities.Movie;

import java.util.Set;

public interface FranchiseService extends CrudService<Franchise, Integer> {
    public void updateMovies(int franchiseId, Set<Movie> movies);
    public Set<Movie> findAllMovies(int franchiseId);
}
