package no.noroff.accelerate22.assignment3.services;

import no.noroff.accelerate22.assignment3.models.entities.Character;
import no.noroff.accelerate22.assignment3.models.entities.Franchise;
import no.noroff.accelerate22.assignment3.models.entities.Movie;
import no.noroff.accelerate22.assignment3.utils.exceptions.MovieNotFoundException;
import no.noroff.accelerate22.assignment3.repositories.CharacterRepository;
import no.noroff.accelerate22.assignment3.repositories.FranchiseRepository;
import no.noroff.accelerate22.assignment3.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService {
    private final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);
    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;
    private final FranchiseRepository franchiseRepository;

    public MovieServiceImpl(MovieRepository movieRepository,
                            CharacterRepository characterRepository, FranchiseRepository franchiseRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
        this.franchiseRepository = franchiseRepository;
    }

    /**
     * Finds a Movie by its ID
     *
     * @param id movie ID that should be found
     * @throws MovieNotFoundException if a movie with given id doesn't exist
     * @return Movie object
     */
    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElseThrow(() -> new MovieNotFoundException(id));
    }

    /**
     * Finds all movies from the movie table.
     *
     * @return a collection of Movie objects
     */
    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    /**
     * Adds a new movie to the database.
     *
     * @param entity an object representation of the new movie
     * @return a Movie object
     */
    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    /**
     * Updates an existing movie.
     *
     * @param entity an object representation of a movie that should be updated, containing the new values
     * @return the updated Movie object
     */
    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    /**
     * Deletes a movie by its ID.
     *
     * @param id movie ID that should be deleted
     */
    @Override
    public void deleteById(Integer id) {
        deleteMovieReference(id);
        movieRepository.deleteById(id);
    }

    /**
     * Deletes a movie by its object representation
     *
     * @param entity an object representation of a movie that should be deleted
     */
    @Override
    public void delete(Movie entity) {
        deleteMovieReference(entity.getId());
        movieRepository.delete(entity);
    }

    /**
     * Sets a new reference of movie franchise.
     *
     * @param movieId a movie whose franchise should be set
     * @param franchise object representation of Franchise (can be null, then the reference is being removed)
     */
    @Override
    public void setMovieFranchise(int movieId, Franchise franchise) {
        Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new MovieNotFoundException(movieId));
        if (franchise != null) {
            franchise.getMovies().add(movie);
            franchiseRepository.save(franchise);
        } else {
            // remove reference
            Franchise movieFranchise = movie.getFranchise();
            movieFranchise.getMovies().remove(movie);
            franchiseRepository.save(movieFranchise);
        }
        movie.setFranchise(franchise);
        movieRepository.save(movie);

    }

    /**
     * Finds all characters that appear in a given movie
     *
     * @param movieId movie id whose characters should be found
     * @return a set of Character objects
     */
    @Override
    public Set<Character> findAllCharacters(int movieId) {
        Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new MovieNotFoundException(movieId));
        return movie.getCharacters();
    }

    /**
     * Updates the set set of characters that appear in a given movie.
     *
     * @param movieId movie id whose characters should be updated
     * @param characters a set of Character objects that will replace the old reference
     */
    @Override
    public void updateCharacters(int movieId, Set<Character> characters) {
        Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new MovieNotFoundException(movieId));
        movie.setCharacters(characters);
        movie.getCharacters().forEach(ch -> {
            Set<Movie> movies = ch.getMovies();
            if (movies == null) {
                movies = Set.of(movie);
            } else {
                movies.add(movie);
            }
            ch.setMovies(movies);
            characterRepository.save(ch);
        });
        movieRepository.save(movie);
    }

    /**
     * Deletes movie reference from character object, in case of movie deletion.
     *
     * @param movieId movie ID that should be deleted
     */
    private void deleteMovieReference(int movieId) {
        Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new MovieNotFoundException(movieId));
        movie.getCharacters().forEach(character -> {
            Set<Movie> movies = character.getMovies();
            movies.remove(movie);
            character.setMovies(movies);
            characterRepository.save(character);
        });
    }
}
