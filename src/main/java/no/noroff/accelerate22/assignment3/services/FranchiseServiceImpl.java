package no.noroff.accelerate22.assignment3.services;

import no.noroff.accelerate22.assignment3.models.entities.Franchise;
import no.noroff.accelerate22.assignment3.models.entities.Movie;
import no.noroff.accelerate22.assignment3.utils.exceptions.FranchiseNotFoundException;
import no.noroff.accelerate22.assignment3.repositories.FranchiseRepository;
import no.noroff.accelerate22.assignment3.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
public class FranchiseServiceImpl implements FranchiseService {
    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    /**
     * Finds a franchise by its ID
     *
     * @param id
     * @return Franchise object
     */
    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
    }


    /**
     * Finds all franchises from the franchise table
     *
     * @return a collection of Franchise object
     */
    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    /**
     * Adds a new franchise to the database.
     *
     * @param entity an object representation of a franchise
     * @return a Franchise object
     */
    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    /**
     * Updates an existing Franchise
     *
     * @param entity an object representation of a franchise, containing new values
     * @return
     */
    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    /**
     * Deletes a franchise by its ID
     *
     * @param id franchise ID
     */
    @Override
    public void deleteById(Integer id) {
        removeFranchiseReferenceFromMovies(id);
        franchiseRepository.deleteById(id);
    }

    /**
     * Deletes a franchise based on its object representation
     *
     * @param franchise a Franchise object that should be deleted
     */
    @Override
    public void delete(Franchise franchise) {
        // delete franchise references from movies
        removeFranchiseReferenceFromMovies(franchise.getId());
        franchiseRepository.delete(franchise);
    }

    /**
     * Updates a list of movies that are being own by the franchise
     *
     * @param franchiseId franchise ID whose movies should be updated
     * @param movies movies that should replace the old set of movies
     */
    @Override
    public void updateMovies(int franchiseId, Set<Movie> movies) {
        Franchise franchise = franchiseRepository.findById(franchiseId).orElseThrow(() -> new FranchiseNotFoundException(franchiseId));
        franchise.setMovies(movies);
        franchise.getMovies().forEach(movie -> {
            movie.setFranchise(franchise);
            movieRepository.save(movie);
        });
        franchiseRepository.save(franchise);
    }

    /**
     * Finds all movies that are being own by the franchise
     *
     * @param franchiseId franchise ID whose movies should be found
     * @return set of Movie objects
     */
    @Override
    public Set<Movie> findAllMovies(int franchiseId) {
        Franchise franchise = franchiseRepository.findById(franchiseId).orElseThrow(() -> new FranchiseNotFoundException(franchiseId));
        return franchise.getMovies();
    }

    /**
     * Removes franchise reference from Movie objects in case of Franchise deletion
     *
     * @param franchiseId franchise id that should be deleted
     */
    private void removeFranchiseReferenceFromMovies(int franchiseId) {
        Collection<Movie> franchiseMovies = movieRepository.findMoviesFromFranchise(franchiseId);
        if (franchiseMovies != null) {
            franchiseMovies.forEach(movie -> {
                        movie.setFranchise(null);
                        movieRepository.save(movie);
                    }
            );
        }
    }
}
