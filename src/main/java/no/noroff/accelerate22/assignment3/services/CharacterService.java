package no.noroff.accelerate22.assignment3.services;

import no.noroff.accelerate22.assignment3.models.entities.Character;
import no.noroff.accelerate22.assignment3.models.entities.Movie;

import java.util.Set;

public interface CharacterService extends CrudService<Character, Integer> {
    public void updateMovies(int characterId, Set<Movie> movies);
    public Set<Movie> findAllMovies(int characterId);
}
