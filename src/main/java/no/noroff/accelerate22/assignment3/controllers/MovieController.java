package no.noroff.accelerate22.assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate22.assignment3.models.DTOs.FranchiseDTO;
import no.noroff.accelerate22.assignment3.models.DTOs.MovieDTO;
import no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs.MovieCharacterDTO;
import no.noroff.accelerate22.assignment3.models.entities.Movie;
import no.noroff.accelerate22.assignment3.utils.error.ApiErrorResponse;
import no.noroff.accelerate22.assignment3.mappers.FranchiseMapper;
import no.noroff.accelerate22.assignment3.mappers.MovieMapper;
import no.noroff.accelerate22.assignment3.services.CharacterService;
import no.noroff.accelerate22.assignment3.services.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {
    private final MovieService movieService;
    private final CharacterService characterService;
    private final FranchiseMapper franchiseMapper;
    private final MovieMapper movieMapper;

    public MovieController(MovieService movieService,
                           FranchiseMapper franchiseMapper,
                           MovieMapper movieMapper,
                           CharacterService characterService) {
        this.movieService = movieService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
        this.characterService = characterService;
    }

    @Operation(summary = "Get all movies")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    ))
    })
    @GetMapping
    public ResponseEntity<Collection<MovieDTO>> getAll() {
        Collection<MovieDTO> movies = movieMapper.movieToMovieDto(movieService.findAll());
        return ResponseEntity.ok(movies);
    }

    @Operation(summary = "Get a movie by its ID")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @GetMapping("{id}")
    public ResponseEntity<MovieDTO> getById(@PathVariable int id) {
        return ResponseEntity.ok(movieMapper.movieToMovieDto(movieService.findById(id)));
    }

    @Operation(summary = "Add a new movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "Created",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @PostMapping
    public ResponseEntity<Movie> add(@RequestBody MovieDTO movieDto) {
        Movie m = movieService.add(movieMapper.movieDtoToMovie(movieDto));
        URI location = URI.create("movies/" + m.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @PutMapping("{id}")
    public ResponseEntity<Movie> update(@RequestBody MovieDTO movieDto, @PathVariable int id) {
        if (id != movieDto.getId()) {
            return ResponseEntity.badRequest().build();
        }
        movieService.update(movieMapper.movieDtoToMovie(movieDto));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a movie by its ID")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @DeleteMapping("{id}")
    public ResponseEntity<Movie> deleteById(@PathVariable int id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a movie by its object representation")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @DeleteMapping
    public ResponseEntity<Movie> delete(@RequestBody MovieDTO movieDto) {
        movieService.delete(movieMapper.movieDtoToMovie(movieDto));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Update the list of characters that appear in a given movie. It will also update the references in character objects.")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @PutMapping("{id}/characters")
    public ResponseEntity<Movie> updateCharacters(@PathVariable int id, @RequestBody Set<MovieCharacterDTO> charactersIds) {
        // convert domain DTOs to Movie objects
        movieService.updateCharacters(id, charactersIds.stream().map(dto -> characterService.findById(dto.getId())).collect(Collectors.toSet()));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Update movie franchise. It will also update references in franchise object")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @PutMapping("{id}/franchise")
    public ResponseEntity<Movie> updateFranchise(@PathVariable int id, @RequestBody FranchiseDTO franchiseDto) {
        movieService.setMovieFranchise(id, franchiseMapper.franchiseDtoToFranchise(franchiseDto));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get a list of all characters that appear in a given movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @GetMapping("{id}/characters")
    public ResponseEntity<Collection<MovieCharacterDTO>> getAllCharacters(@PathVariable int id) {
        return ResponseEntity.ok(movieService.findAllCharacters(id).stream().map(m -> {
            // convert Movie objects to domain DTOs that only contain character IDs
            MovieCharacterDTO dto = new MovieCharacterDTO();
            dto.setId(m.getId());
            return dto;
        }).collect(Collectors.toSet()));
    }
}