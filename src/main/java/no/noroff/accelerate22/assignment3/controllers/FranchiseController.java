package no.noroff.accelerate22.assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate22.assignment3.models.DTOs.FranchiseDTO;
import no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs.FranchiseMovieDTO;
import no.noroff.accelerate22.assignment3.models.entities.Franchise;
import no.noroff.accelerate22.assignment3.utils.error.ApiErrorResponse;
import no.noroff.accelerate22.assignment3.mappers.FranchiseMapper;
import no.noroff.accelerate22.assignment3.services.FranchiseService;
import no.noroff.accelerate22.assignment3.services.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path="api/v1/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final MovieService movieService;
    private final FranchiseMapper franchiseMapper;

    public FranchiseController(
            FranchiseService franchiseService,
            FranchiseMapper franchiseMapper,
            MovieService movieService
    ) {

        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieService = movieService;
    }

    @Operation(summary = "Get all franchises")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @GetMapping
    public ResponseEntity<Collection<FranchiseDTO>> getAll() {
        return ResponseEntity.ok(franchiseMapper.franchiseToFranchiseDto(franchiseService.findAll()));
    }

    @Operation(summary = "Get a franchise by its ID")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @GetMapping("{id}")
    public ResponseEntity<FranchiseDTO> getById(@PathVariable int id) {
        return ResponseEntity.ok(franchiseMapper.franchiseToFranchiseDto(franchiseService.findById(id)));
    }

    @Operation(summary = "Add a new franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "Created",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @PostMapping
    public ResponseEntity<Franchise> add(@RequestBody FranchiseDTO franchiseDto) {
        Franchise f = franchiseService.add(franchiseMapper.franchiseDtoToFranchise(franchiseDto));
        URI location = URI.create("franchises/" + f.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @PutMapping("{id}")
    public ResponseEntity<Franchise> update(@PathVariable int id, @RequestBody FranchiseDTO franchiseDto) {
        if (id != franchiseDto.getId()) {
            return ResponseEntity.badRequest().build();
        }
        franchiseService.update(franchiseMapper.franchiseDtoToFranchise(franchiseDto));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a franchise by its ID")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @DeleteMapping("{id}")
    public ResponseEntity<Franchise> deleteById(@PathVariable int id) {
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a franchise by its object representation")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @DeleteMapping
    public ResponseEntity<Franchise> delete(@RequestBody FranchiseDTO franchiseDto) {
        franchiseService.delete(franchiseMapper.franchiseDtoToFranchise(franchiseDto));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Update the movies that are owned by a given franchise. It will also references in movie objects")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @PutMapping("{id}/movies")
    public ResponseEntity<Franchise> updateMovies(@PathVariable int id, @RequestBody Set<FranchiseMovieDTO> movieIds) {
        // convert domain DTOs (only containing the IDs) to Movie objects
        franchiseService.updateMovies(id, movieIds.stream().map(dto -> movieService.findById(dto.getId())).collect(Collectors.toSet()));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get a list of all movies owned by a given franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @GetMapping("{id}/movies")
    public ResponseEntity<Collection<FranchiseMovieDTO>> getAllMovies(@PathVariable int id) {
        return ResponseEntity.ok(franchiseService.findAllMovies(id).stream().map(m -> {
            // convert movie objects to domain DTOs (only the IDs)
            FranchiseMovieDTO dto = new FranchiseMovieDTO();
            dto.setId(m.getId());
            return dto;
        }).collect(Collectors.toSet()));
    }
}
