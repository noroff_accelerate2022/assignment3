package no.noroff.accelerate22.assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate22.assignment3.models.DTOs.CharacterDTO;
import no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs.CharacterMovieDTO;
import no.noroff.accelerate22.assignment3.models.entities.Character;
import no.noroff.accelerate22.assignment3.utils.error.ApiErrorResponse;
import no.noroff.accelerate22.assignment3.mappers.CharacterMapper;
import no.noroff.accelerate22.assignment3.services.CharacterService;
import no.noroff.accelerate22.assignment3.services.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path="api/v1/characters")
public class CharacterController {
    private final CharacterService characterService;
    private final MovieService movieService;
    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper, MovieService movieService) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
        this.movieService = movieService;
    }

    @Operation(summary = "Get all characters")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                        mediaType = "application/json",
                        schema = @Schema(implementation = CharacterDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @GetMapping
    public ResponseEntity<Collection<CharacterDTO>> getAll() {
        return ResponseEntity.ok(characterMapper.characterToCharacterDto(characterService.findAll()));
    }

    @Operation(summary = "Get a character by its ID")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @GetMapping("{id}")
    public ResponseEntity<CharacterDTO> getById(@PathVariable int id) {
        return ResponseEntity.ok(characterMapper.characterToCharacterDto(characterService.findById(id)));
    }

    @Operation(summary = "Add a new character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "Created",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    ))
    })
    @PostMapping
    public ResponseEntity<Character> add(@RequestBody CharacterDTO characterDto) {
        Character c = characterService.add(characterMapper.characterDtoToCharacter(characterDto));
        URI location = URI.create("characters/" + c.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update an existing character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @PutMapping("{id}")
    public ResponseEntity<Character> update(@RequestBody CharacterDTO characterDto, @PathVariable int id) {
        if (id != characterDto.getId())
            return ResponseEntity.badRequest().build();
        characterService.update(characterMapper.characterDtoToCharacter(characterDto));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a character by its ID")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @DeleteMapping("{id}")
    public ResponseEntity<Character> deleteById(@PathVariable int id) {
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a character by its object representation")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @DeleteMapping
    public ResponseEntity<Character> delete(@RequestBody CharacterDTO characterDto) {
        characterService.delete(characterMapper.characterDtoToCharacter(characterDto));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Update movies the character plays in. It will also update references in movie objects.")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "No content",
                    content = {@Content}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @PutMapping("{id}/movies")
    public ResponseEntity<Character> updateMovies(@PathVariable int id, @RequestBody Set<CharacterMovieDTO> movieIds) {
        // extract movie IDs as integers to get the Movie objects
        characterService.updateMovies(id, movieIds.stream().map(dto -> movieService.findById(dto.getId())).collect(Collectors.toSet()));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get all movies the character plays in")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema=@Schema(implementation = ApiErrorResponse.class)
                    )),
    })
    @GetMapping("{id}/movies")
    public ResponseEntity<Collection<CharacterMovieDTO>> getAllMovies(@PathVariable int id) {
        return ResponseEntity.ok(characterService.findAllMovies(id).stream().map(m -> {
            // convert Movie objects to domain DTOs that only contain the movie IDs
            CharacterMovieDTO dto = new CharacterMovieDTO();
            dto.setId(m.getId());
            return dto;
        }).collect(Collectors.toSet()));
    }
}
