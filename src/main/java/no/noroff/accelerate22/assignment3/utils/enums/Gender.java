package no.noroff.accelerate22.assignment3.utils.enums;

public enum Gender {
    FEMALE,
    MALE,
    OTHER
}
