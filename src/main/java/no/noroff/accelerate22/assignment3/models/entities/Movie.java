package no.noroff.accelerate22.assignment3.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private int id;
    @Column(nullable = false)
    private String title;
    @Column(length = 512)
    private String genre;
    private int releaseYear;
    private String director;
    private String picture;
    private String trailer;

    @ManyToMany(mappedBy = "movies")
    private Set<Character> characters;
    @ManyToOne
    @JoinColumn(name="franchise_id")
    private Franchise franchise;
}