package no.noroff.accelerate22.assignment3.models.DTOs;

import lombok.Data;
import no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs.MovieCharacterDTO;

import java.util.Set;

@Data
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private Integer releaseYear;
    private String director;
    private String picture;
    private String trailer;
    private Set<MovieCharacterDTO> characters;
    private Integer franchise;
}
