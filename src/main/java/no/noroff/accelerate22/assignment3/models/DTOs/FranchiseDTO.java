package no.noroff.accelerate22.assignment3.models.DTOs;

import lombok.Data;
import no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs.FranchiseMovieDTO;

import java.util.Set;

@Data
public class FranchiseDTO {
    private int id;
    private String name;
    private String description;
    private Set<FranchiseMovieDTO> movies;
}
