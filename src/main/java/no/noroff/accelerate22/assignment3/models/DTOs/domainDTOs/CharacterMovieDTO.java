package no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs;

import lombok.Data;

/**
 * A representation of movie references
 * that are being stored in Character objects.
 */
@Data
public class CharacterMovieDTO {
    private int id;
}
