package no.noroff.accelerate22.assignment3.models.entities;

import lombok.*;
import no.noroff.accelerate22.assignment3.utils.enums.Gender;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private int id;
    @Column(nullable = false)
    private String fullName;
    private String alias;
    @Column(length = 8)
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private String picture;

    @ManyToMany
    @JoinTable(
            name = "character_movies",
            joinColumns = { @JoinColumn(name = "character_id") },
            inverseJoinColumns = { @JoinColumn(name = "movie_id") }
    )
    @ToString.Exclude
    private Set<Movie> movies;
}
