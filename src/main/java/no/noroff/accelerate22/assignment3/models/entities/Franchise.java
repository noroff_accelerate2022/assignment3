package no.noroff.accelerate22.assignment3.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "franchise_id")
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(length = 512)
    private String description;
    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
}
