package no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs;

import lombok.Data;

/**
 * A representation of movie references
 * that are being stored in Franchise object.
 */
@Data
public class FranchiseMovieDTO {
    private int id;
}
