package no.noroff.accelerate22.assignment3.models.DTOs;

import lombok.Data;
import no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs.CharacterMovieDTO;
import no.noroff.accelerate22.assignment3.utils.enums.Gender;

import java.util.Set;

@Data
public class CharacterDTO {
    private int id;
    private String fullName;
    private String alias;
    private Gender gender;
    private String Picture;
    private Set<CharacterMovieDTO> movies;
}
