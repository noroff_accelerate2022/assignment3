package no.noroff.accelerate22.assignment3.models.DTOs.domainDTOs;

import lombok.Data;

/**
 * A representation of character references
 * that are being stored in Movie objects.
 */
@Data
public class MovieCharacterDTO {
    private int id;
}
