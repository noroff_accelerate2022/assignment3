package no.noroff.accelerate22.assignment3.repositories;

import no.noroff.accelerate22.assignment3.models.entities.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer> {
}
