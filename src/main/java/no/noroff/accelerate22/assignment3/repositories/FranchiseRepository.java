package no.noroff.accelerate22.assignment3.repositories;

import no.noroff.accelerate22.assignment3.models.entities.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
}
