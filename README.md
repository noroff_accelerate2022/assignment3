# Assignment 3 - MediaDB Web API

A simple web API for media database containing movies, characters and franchises.

The appliction is contanerized and deployed on heroku, and currently the only page that could be accessed is the API documentation created with Swagger. The docs can be found [here](https://assignment3-media-db.herokuapp.com/swagger-ui/index.html).

The API models the various entities exisitng in the database, i.e., movies, characters and franchises. There are certain relationships between those entities, and the API offers various ways to view and manipulate the data, using the standard CRUD interface, as well as domain-specific methods. 

### Using the API

In order to test the API, one can send requests using an external tool, e.g. Postman, with `assignment3-media-db.herokuapp.com/` domain name. For example, this `GET` request `assignment3-media-db.herokuapp.com/api/v1/movies` will return all the movies that are currently in the heroku database (if SSL verification is switched off in Postman, one should also be able to access the API via HTTPS). It is also possible to test the API locally, as the application has different set of configurations for dev environment. In order to do that, one should create a local `postgresql` database beforehand, and make sure that the credentials are the same as in the `application.properties` file. The application will then seed some dummy data to the database on each launch.

### CI/CD

The application has a CI/CD pipeline, which archives an artifact of docker build that can be downloaded. It also deploys the newest snapshot to heroku. The pipeline is triggered on each push/merge request to the main branch.
